local resolarized = require 'resolarized'
local palette = resolarized.palette.selenized.black
local scheme = resolarized.scheme.selenized
resolarized.apply('selenized-black', palette, scheme)

