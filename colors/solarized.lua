local resolarized = require 'resolarized'
local palette = resolarized.palette.solarized[vim.o.background]
local scheme = resolarized.scheme.solarized
resolarized.apply('solarized', palette, scheme)
