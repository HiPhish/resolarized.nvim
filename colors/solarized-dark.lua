local resolarized = require 'resolarized'
local palette = resolarized.palette.solarized.dark
local scheme = resolarized.scheme.solarized
resolarized.apply('solarized-dark', palette, scheme)
