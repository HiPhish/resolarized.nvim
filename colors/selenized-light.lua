local resolarized = require 'resolarized'
local palette = resolarized.palette.selenized.light
local scheme = resolarized.scheme.selenized
resolarized.apply('selenized-light', palette, scheme)
