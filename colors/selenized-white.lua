local resolarized = require 'resolarized'
local palette = resolarized.palette.selenized.white
local scheme = resolarized.scheme.selenized
resolarized.apply('selenized-white', palette, scheme)
