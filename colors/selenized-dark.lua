local resolarized = require 'resolarized'
local palette = resolarized.palette.selenized.dark
local scheme = resolarized.scheme.selenized
resolarized.apply('selenized-dark', palette, scheme)
