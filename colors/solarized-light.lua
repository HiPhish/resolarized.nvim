local resolarized = require 'resolarized'
local palette = resolarized.palette.solarized.light
local scheme = resolarized.scheme.solarized
resolarized.apply('solarized-light', palette, scheme)
