local M = {}

local keys = {
	'fg', 'bg', 'sp',
	'blend',
	'bold', 'italic',
	'standout', 'strikethrough', 'reverse',
	'underline', 'undercurl', 'underdouble', 'underdotted', 'underdashed',
	'nocombine',
	'default',
	'ctermfg', 'ctermbg', 'cterm',
}

---Given a combined gui/cterm entry unpack it into separate gui- and cterm
---values
local function unpack_palette(kwargs, key, guikey, ctermkey)
	local guivalue = kwargs[key].gui
	local ctervalue = kwargs[key].cterm
	kwargs[guikey] = guivalue
	kwargs[ctermkey] = ctervalue
end

local function unpack_key(kwargs, palette, key, guikey, ctermkey)
	local entry = palette[kwargs[key]]
	if not entry then return end
	local guivalue = entry.gui
	local ctervalue = entry.cterm
	kwargs[guikey] = guivalue
	kwargs[ctermkey] = ctervalue
end

--- Traverse a list of specification, executing each specification as it is
--- visited. Recursively descends into child-specifications.
---
--- @param group string         # Name of the group
--- @param spec table | string  # Highlight specification of the group
local function traverse_spec(group, spec, palette)
	if type(spec) == 'string' then
		vim.api.nvim_set_hl(0, group, {link=spec})
		return
	end

	--Value table of highlight options
	local kwargs = {}
	for _, key in ipairs(keys) do
		kwargs[key] = spec[key]
	end

	if type(kwargs.fg) == 'table' then
		unpack_palette(kwargs, 'fg', 'fg', 'ctermfg')
	elseif type(kwargs.fg) == 'string' then
		unpack_key(kwargs, palette, 'fg', 'fg', 'ctermfg')
	end

	if type(kwargs.bg) == 'table' then
		unpack_palette(kwargs, 'bg', 'bg', 'ctermbg')
	elseif type(kwargs.bg) == 'string' then
		unpack_key(kwargs, palette, 'bg', 'bg', 'ctermfg')
	end

	if type(kwargs.sp) == 'table' then
		kwargs.sp = kwargs.sp.gui
	elseif type(kwargs.spl) == 'string' then
		kwargs.sp = palette[kwargs.sp].gui
	end

	local children = rawget(spec, 'children')
	if children then
		kwargs.children = nil
		local child_mt = {__index = spec}
		for child, child_spec in pairs(children) do
			setmetatable(child_spec, child_mt)
			traverse_spec(child, child_spec, palette)
		end
	end

	vim.api.nvim_set_hl(0, group, kwargs)
end

--- @alias spec 'string | table<string, spec>'

--- Applies a Resolarized colour scheme according to the given scheme
--- specification.
---
--- @param name    string               # sets `g:colors_name` to this value
--- @param palette table<string, spec>  # Palette to use
--- @param scheme  table<string, spec>  # Colour theme specification
function M.apply(name, palette, scheme)
	vim.cmd {cmd='highlight', args={'clear'}}
	vim.g.colors_name = name

	for group, spec in pairs(scheme.hlgroups) do
		 traverse_spec(group, spec, palette)
	end

	for i, key in ipairs(scheme.termcolors) do
		vim.g[string.format('terminal_color%d', i - 1)] = palette[key].str
	end
end

M.palette = {
	solarized = require 'resolarized._palette.solarized',
	selenized = require 'resolarized._palette.selenized',
}

M.scheme = {
	solarized = require 'resolarized._scheme.solarized',
	selenized = require 'resolarized._scheme.selenized',
}

return M
