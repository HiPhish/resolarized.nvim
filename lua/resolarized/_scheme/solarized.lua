---The highlight scheme for Solarized.
local M = {}

M.hlgroups = {
	-- Semantic highlight groups, see `:h group-name`
	Comment    = {fg='base01', italic=true},

	Constant   = {fg='cyan'},
	String     = 'Constant',
	Character  = 'Constant',
	Number     = 'Constant',
	Boolean    = 'Constant',
	Float      = 'Constant',

	Identifier = {fg='blue'},
	Function   = {fg='blue'},

	Statement   = {fg='green'},
	Conditional = 'Statement',
	Repeat      = 'Statement',
	Label       = 'Statement',
	Operator    = 'Statement',
	Keyword     = 'Statement',
	Exception   = 'Statement',

	PreProc   = {fg='orange'},
	Include   = 'PreProc',
	Define    = 'PreProc',
	Macro     = 'PreProc',
	PreCondit = 'PreProc',

	Type         = {fg='yellow'},
	StorageClass = 'Type',
	Structure    = 'Type',
	Typedef      = 'Type',

	Special        = {fg='orange'},
	SpecialChar    = 'Special',
	Tag            = 'Special',
	Delimiter      = 'Special',
	SpecialComment = 'Special',
	Debug          = 'Special',

	Underlined = {fg='blue'   , underline=true},
	Ignore     = {},
	Error      = {fg='red'    , bold=true, italic=true},
	Todo       = {fg='yellow' , bold=true},

	Added = {fg='green'},
	Changed = {fg='yellow'},
	Removed = {fg='red'},

	-- Default Vim highlighting groups, see `:h highlight-default`
	ColorColumn  = {             bg='base03'},
	Conceal      = {fg='blue'               },
	CurSearch    = {fg='base03', bg='orange'},
	Cursor       = {fg='base03', bg='base0' },
	lCursor      = 'Cursor',
	TermCursor   = 'Cursor',
	TermCursorNC = {fg='base03', bg='base01'},
	CursorIM     = {fg='base03', bg='base1' },
	CursorColumn = {             bg='base02'},
	CursorLine   = 'CursorColumn',
	Directory    = {fg='blue'               },
	DiffAdd      = {fg='green',  bg='base02', sp='green'},
	DiffChange   = {fg='yellow', bg='base02', sp='yellow'},
	DiffDelete   = {fg='red',    bg='base02', sp='red'},
	DiffText     = {fg='yellow', bg='base02', underdotted=true, cterm={bold=true, underline=true}},
	EndOfBuffer  = 'NonText',
	ErrorMsg     = {fg='red',                 bold=true, cterm={}},
	WinSeparator = {fg='base01', bg='base02'},
	VertSplit    = 'WinSeparator',
	Folded       = {fg='base01', bg='base02'},
	FoldColumn   = {fg='base01', bg='base02'},
	SignColumn   = 'LineNr',
	IncSearch    = 'CurSearch',
	Substitute   = 'Search',
	-- Maybe use a subdued background here?
	LineNr       = {fg='base01', bg='base04'},
	CursorLineNr = {fg='base0',  bg='base02', bold=true},
	MatchParen   = {bold=true, underline=true},
	ModeMsg      = {fg='blue'               },
	MsgArea      = 'Normal',
	MsgSeparator = {fg='base01'             },
	MoreMsg      = {fg='blue'               },
	NonText      = {fg='base01'             },
	Normal       = {fg='base0',  bg='base03'},
	NormalFloat  = 'Pmenu',
	NormalNC     = 'Normal',
	Pmenu        = {fg='base0',  bg='base02'},
	PmenuSel     = {fg='base0',  bg='base01', bold=true},
	PmenuSbar    = {             bg='base01'},
	PmenuThumb   = {             bg='base0' },
	Question     = {fg='cyan',                bold=true},
	QuickFixLine = {fg='base03', bg='base01'},
	Search       = {fg='base03', bg='yellow'},
	SpecialKey   = {                          bold=true, italic=true},
	SpellBad     = {
		sp = 'violet', undercurl=true,
		children = {
			SpellCap   = {sp='violet'},
			SpellLocal = {sp='yellow'},
			SpellRare  = {sp='cyan'},
		}
	},
	StatusLine   = {fg='base0' , bg = 'base02'},
	StatusLineNC = {fg='base01', bg = 'base02'},
	TabLine      = 'StatusLine',
	TabLineFill  = 'StatusLineNC',
	TabLineSel   = {fg='base03', bg = 'base01', bold=true},
	Title        = {fg='orange',                bold=true},
	Visual       = {fg='base03', bg = 'base01'},
	VisualNOS    = 'Visual',
	WarningMsg   = {fg='yellow',                bold=true},
	Whitespace   = 'NonText',
	WildMenu     = {fg='base1', bg = 'base02',  bold=true},
	WinBar       = 'StatusLine',
	WinBarNC     = 'StatusLineNC',


	-- Language Server Protocol, see `:h lsp-highlight`
	LspReferenceText = {
		bg = 'yellow', fg='base03',
		children = {
			LspReferenceRead = {bg='cyan'},
			LspReferenceWrite = {bg='magenta'},
		}
	},
	-- I have no idea what code lenses are, but making it a comment is a good
	-- bet
	LspCodeLens = 'Comment',


	-- Diagnostic, see `:h diagnostic-highlights`
	DiagnosticOk = {
		fg = 'green', bold = true,
		children = {
			DiagnosticVirtualTextOk = {italic=true},
			DiagnosticUnderlineOk = {
				fg = 'NONE', undercurl=true
			},
			--DiagnosticFloatingOk = {},
			DiagnosticSignOk = {bg='base04'},
		}
	},
	DiagnosticError = {
		fg = 'red', sp = 'red',
		children = {
			DiagnosticVirtualTextError = {italic=true},
			DiagnosticUnderlineError = {
				fg = 'NONE', undercurl=true
			},
			--DiagnosticFloatingError = {},
			DiagnosticSignError = {bg='base04'},
		}
	},
	DiagnosticWarn ={
		fg = 'yellow', sp = 'yellow',
		children = {
			DiagnosticVirtualTextWarn = {italic=true},
			DiagnosticUnderlineWarn = {
				fg='NONE', undercurl=true
			},
			--DiagnosticFloatingWarn = {},
			DiagnosticSignWarn = {bg='base04'},
		}
	},
	DiagnosticInfo = {
		fg = 'magenta', sp = 'magenta',
		children = {
			DiagnosticVirtualTextInfo = {italic=true},
			DiagnosticUnderlineInfo = {
				fg = 'NONE', undercurl=true,
			},
			--DiagnosticFloatingInfo = {},
			DiagnosticSignInfo = {bg='base04'},
		}
	},
	DiagnosticHint = {
		fg = 'violet', sp = 'violet',
		children = {
			DiagnosticVirtualTextHint = {italic=true},
			DiagnosticUnderlineHint = {
				fg = 'NONE', undercurl=true,
			},
			--DiagnosticFloatingHint = {},
			DiagnosticSignHint = {bg='base04'},
		}
	},

	-- Tree-sitter, see `:h treesitter-highlight-groups`

	['@variable'] = {fg = 'base0'},
	-- ['@variable.builtin']
	-- ['@variable.parameter']
	-- ['@variable.parameter.builtin']
	-- ['@variable.member']

	-- ['@constant']
	-- ['@constant.builtin']
	-- ['@constant.macro']

	-- ['@module']
	-- ['@module.builtin']
	-- ['@label']

	-- ['@string']
	-- ['@string.documentation']
	-- ['@string.regexp']
	-- ['@string.escape']
	-- ['@string.special']
	-- ['@string.special.symbol']
	-- ['@string.special.path']
	-- ['@string.special.url']

	-- ['@character']
	-- ['@character.special']

	-- ['@boolean']
	-- ['@number']
	-- ['@number.float']

	-- ['@type']
	-- ['@type.builtin']
	-- ['@type.definition']

	-- ['@attribute']
	-- ['@attribute.builtin']
	-- ['@property']

	-- ['@function']
	-- ['@function.builtin']
	-- ['@function.call']
	-- ['@function.macro']

	-- ['@function.method']
	-- ['@function.method.call']

	-- ['@constructor']
	-- ['@operator']

	-- ['@keyword']
	-- ['@keyword.coroutine']
	-- ['@keyword.function']
	-- ['@keyword.operator']
	-- ['@keyword.import']
	-- ['@keyword.type']
	-- ['@keyword.modifier']
	-- ['@keyword.repeat']
	-- ['@keyword.return']
	-- ['@keyword.debug']
	-- ['@keyword.exception']

	-- ['@keyword.conditional']
	-- ['@keyword.conditional.ternary']

	-- ['@keyword.directive']
	-- ['@keyword.directive.define']

	-- ['@punctuation.delimiter']
	-- ['@punctuation.bracket']
	-- ['@punctuation.special']

	-- ['@comment']
	-- ['@comment.documentation']

	-- ['@comment.error']
	-- ['@comment.warning']
	-- ['@comment.todo']
	-- ['@comment.note']

	-- ['@markup.strong']
	-- ['@markup.italic']
	-- ['@markup.strikethrough']
	-- ['@markup.underline']

	-- ['@markup.heading']
	-- ['@markup.heading.1']
	-- ['@markup.heading.2']
	-- ['@markup.heading.3']
	-- ['@markup.heading.4']
	-- ['@markup.heading.5']
	-- ['@markup.heading.6']

	-- ['@markup.quote']
	-- ['@markup.math']

	-- ['@markup.link']
	-- ['@markup.link.label']
	-- ['@markup.link.url']

	-- ['@markup.raw']
	-- ['@markup.raw.block']

	-- ['@markup.list']
	-- ['@markup.list.checked']
	-- ['@markup.list.unchecked']

	-- ['@diff.plus']
	-- ['@diff.minus']
	-- ['@diff.delta']

	-- ['@tag']
	-- ['@tag.builtin']
	-- ['@tag.attribute']
	-- ['@tag.delimiter']

	-- Rainbow (my own extension)
	RainbowRed    = {fg='red'},
	RainbowOrange = {fg='orange'},
	RainbowYellow = {fg='yellow'},
	RainbowGreen  = {fg='green'},
	RainbowCyan   = {fg='cyan'},
	RainbowBlue   = {fg='blue'},
	RainbowViolet = {fg='violet'},

	-- Status line accent colours, my own extension
	StatusLineAccent        = {           fg='base03', bg='base00'},
	StatusLineAccentNormal  = {bold=true, fg='base03', bg='blue'},
	StatusLineAccentInsert  = {bold=true, fg='base03', bg='green'},
	StatusLineAccentReplace = {bold=true, fg='base03', bg='red'},
	StatusLineAccentVisual  = {bold=true, fg='base03', bg='magenta'},
	StatusLineAccentSelect  = {bold=true, fg='base03', bg='violet'},
	StatusLineAccentCmd     = {bold=true, fg='base03', bg='cyan'},
}

M.termcolors = {
	'base02',   -- black
	'red',      -- red
	'green',    -- green
	'yellow',   -- yellow
	'blue',     -- blue
	'magenta',  -- magenta
	'cyan',     -- cyan
	'base2',    -- white
	'base03',   -- bright black
	'orange',   -- bright red
	'base01',   -- bright green
	'base00',   -- bright yellow
	'base0',    -- bright blue
	'violet',   -- bright magenta
	'base1',    -- bright cyan
	'base3',    -- bright white
}

return M
