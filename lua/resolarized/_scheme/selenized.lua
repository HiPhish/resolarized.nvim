---The highlight scheme for Selenized.
local M = {}

M.hlgroups = {
	-- Semantic highlight groups, see `:h group-name`
	Comment    = {fg='dim_0', italic=true},

	Constant   = {fg='cyan'},
	String     = 'Constant',
	Character  = 'Constant',
	Number     = 'Constant',
	Boolean    = 'Constant',
	Float      = 'Constant',

	Identifier = {fg='br_blue'},
	Function   = 'Identifier',

	Statement   = {fg='br_yellow'},
	Conditional = 'Statement',
	Repeat      = 'Statement',
	Label       = 'Statement',
	Operator    = 'Statement',
	Keyword     = 'Statement',
	Exception   = 'Statement',

	PreProc   = {fg='orange'},
	Include   = 'PreProc',
	Define    = 'PreProc',
	Macro     = 'PreProc',
	PreCondit = 'PreProc',

	Type         = {fg='green'},
	StorageClass = 'Type',
	Structure    = 'Type',
	Typedef      = 'Type',

	Special        = {fg='red'},
	SpecialChar    = 'Special',
	Tag            = 'Special',
	Delimiter      = 'Special',
	SpecialComment = 'Special',
	Debug          = 'Special',

	Underlined = {fg='violet' , underline=true},
	Error      = {fg='red'    , bold=true, italic=true},
	Todo       = {fg='magenta', bold=true},
	Ignore     = {},

	Added = {fg='green'},
	Changed = {fg='yellow'},
	Removed = {fg='red'},

	-- Default Vim highlighting groups, see `:h highlight-default`
	ColorColumn  = {              bg='bg_2'  },
	Conceal      = {fg='blue'                },
	CurSearch    = {fg='orange',             reverse=true},
	Cursor       = {reverse = true},
	lCursor      = 'Cursor',
	TermCursor   = 'Cursor',
	TermCursorNC = {fg='bg_0'   , bg='dim_0' },
	CursorIM     = 'Cursor',  -- I have no idea what IME mode is
	CursorColumn = {              bg='bg_1'},
	CursorLine   = 'CursorColumn',
	Directory    = {fg='blue'               },
	DiffAdd      = {fg='green',  bg='bg_1', cterm={bold=true}},
	DiffChange   = {fg='yellow', bg='bg_1', cterm={bold=true}},
	DiffDelete   = {fg='red',    bg='bg_1', cterm={bold=true}},
	DiffText     = {fg='yellow', bg='bg_1', underdotted=true, cterm={bold=true, underline=true}},
	EndOfBuffer  = 'NonText',
	ErrorMsg     = {fg='red',                 bold=true, cterm={}},
	WinSeparator = {fg='dim_0', bg='dim_0'},
	VertSplit    = 'WinSeparator',
	Folded       = {            bg='bg_1'},
	FoldColumn   = {fg='dim_0', bg='bg_2'},
	SignColumn   = 'LineNr',
	IncSearch    = 'CurSearch',
	Substitute   = 'Search',
	-- Maybe use a subdued background here?
	LineNr       = {fg='dim_0'    , bg='bg_1'},
	CursorLineNr = {fg='fg_1'     , bg='bg_1', bold=true},
	MatchParen   = {ft='br_yellow', bg='bg_2', bold=true, underline=true},
	ModeMsg      = {fg='blue'               },
	MsgArea      = 'Normal',
	MsgSeparator = {fg='dim_0'             },
	MoreMsg      = {fg='blue'               , italic=true},
	NonText      = {fg='dim_0'             },
	Normal       = {fg='fg_0',   bg='bg_0'},
	NormalFloat  = 'Pmenu',
	FloatBorder  = 'NormalFloat',
	FloatTitle   = 'NormalFloat',
	NormalNC     = 'Normal',
	Pmenu        = {fg='dim_0',  bg='bg_1'},
	PmenuSel     = {             bg='bg_2'},
	PmenuSbar    = {             bg='bg_2'},
	PmenuThumb   = {             bg='dim_0' },
	Question     = {fg='cyan',                bold=true},
	QuickFixLine = 'Search',
	Search       = {fg='yellow', reverse=true},
	SpecialKey   = {                          bold=true, italic=true},
	SpellBad     = {
		sp='violet', undercurl=true,
		children = {
			SpellCap   = {sp='violet'},
			SpellLocal = {sp='yellow'},
			SpellRare  = {sp='cyan'},
		}
	},
	StatusLine       = {fg='fg_0' , bg='bg_1'},
	StatusLineNC     = {fg='dim_0', bg='bg_1'},
	StatusLineTerm   = 'StatusLine',
	StatusLineTermNC = 'StatusLineNC',
	TabLine          = {fg='dim_0',               reverse=true},
	TabLineFill      = 'TabLine',
	TabLineSel       = {fg='fg_1',   bg='bg_1', bold=true, reverse=true},
	Title            = {fg='orange',            bold=true},
	Visual           = {             bg='bg_2'},
	VisualNOS        = 'Visual',
	WarningMsg       = {fg='yellow',            bold=true},
	Whitespace       = 'NonText',
	WildMenu         = {},
	WinBar           = 'StatusLine',
	WinBarNC         = 'StatusLineNC',

	-- Language Server Protocol, see `:h lsp-highlight`
	LspReferenceText = {
		bg='yellow', fg='bg_0',
		children = {
			LspReferenceRead  = {bg='cyan'},
			LspReferenceWrite = {bg='magenta'},
		}
	},
	-- I have no idea what code lenses are, but making it a comment is a good
	-- bet
	LspCodeLens = 'Comment',


	-- Diagnostic, see `:h diagnostic-highlights`
	DiagnosticOk = {
		fg = 'green', bold = true,
		children = {
			DiagnosticVirtualTextOk = {italic=true},
			DiagnosticUnderlineOk = {
				fg = 'NONE', undercurl=true
			},
			--DiagnosticFloatingOk = {},
			DiagnosticSignOk = {bg='bg_1'},
		}
	},
	DiagnosticError = {
		fg='red', sp='red',
		children = {
			DiagnosticVirtualTextError = {italic=true},
			DiagnosticUnderlineError = {
				fg = 'NONE', undercurl=true
			},
			--DiagnosticFloatingError = {},
			DiagnosticSignError = {bg='bg_1'},
		}
	},
	DiagnosticWarn ={
		fg='yellow', sp='yellow',
		children = {
			DiagnosticVirtualTextWarn = {italic=true},
			DiagnosticUnderlineWarn = {
				fg='NONE', undercurl=true
			},
			--DiagnosticFloatingWarn = {},
			DiagnosticSignWarn = {bg='bg_1'},
		}
	},
	DiagnosticInfo = {
		fg='magenta', sp='magenta',
		children = {
			DiagnosticVirtualTextInfo = {italic=true},
			DiagnosticUnderlineInfo = {
				fg = 'NONE', undercurl=true,
			},
			--DiagnosticFloatingInfo = {},
			DiagnosticSignInfo = {bg='bg_1'},
		}
	},
	DiagnosticHint = {
		fg='violet', sp='violet',
		children = {
			DiagnosticVirtualTextHint = {italic=true},
			DiagnosticUnderlineHint = {
				fg = 'NONE', undercurl=true,
			},
			--DiagnosticFloatingHint = {},
			DiagnosticSignHint = {bg='bg_1'},
		}
	},

	-- Tree-sitter, see `:h treesitter-highlight-groups`

	['@variable'] = {fg = 'fg_0'},
	-- ['@variable.builtin']
	-- ['@variable.parameter']
	-- ['@variable.parameter.builtin']
	-- ['@variable.member']

	-- ['@constant']
	-- ['@constant.builtin']
	-- ['@constant.macro']

	-- ['@module']
	-- ['@module.builtin']
	-- ['@label']

	-- ['@string']
	-- ['@string.documentation']
	-- ['@string.regexp']
	-- ['@string.escape']
	-- ['@string.special']
	-- ['@string.special.symbol']
	-- ['@string.special.path']
	-- ['@string.special.url']

	-- ['@character']
	-- ['@character.special']

	-- ['@boolean']
	-- ['@number']
	-- ['@number.float']

	-- ['@type']
	-- ['@type.builtin']
	-- ['@type.definition']

	-- ['@attribute']
	-- ['@attribute.builtin']
	-- ['@property']

	-- ['@function']
	-- ['@function.builtin']
	-- ['@function.call']
	-- ['@function.macro']

	-- ['@function.method']
	-- ['@function.method.call']

	-- ['@constructor']
	-- ['@operator']

	-- ['@keyword']
	-- ['@keyword.coroutine']
	-- ['@keyword.function']
	-- ['@keyword.operator']
	-- ['@keyword.import']
	-- ['@keyword.type']
	-- ['@keyword.modifier']
	-- ['@keyword.repeat']
	-- ['@keyword.return']
	-- ['@keyword.debug']
	-- ['@keyword.exception']

	-- ['@keyword.conditional']
	-- ['@keyword.conditional.ternary']

	-- ['@keyword.directive']
	-- ['@keyword.directive.define']

	-- ['@punctuation.delimiter']
	-- ['@punctuation.bracket']
	-- ['@punctuation.special']

	-- ['@comment']
	-- ['@comment.documentation']

	-- ['@comment.error']
	-- ['@comment.warning']
	-- ['@comment.todo']
	-- ['@comment.note']

	-- ['@markup.strong']
	-- ['@markup.italic']
	-- ['@markup.strikethrough']
	-- ['@markup.underline']

	-- ['@markup.heading']
	-- ['@markup.heading.1']
	-- ['@markup.heading.2']
	-- ['@markup.heading.3']
	-- ['@markup.heading.4']
	-- ['@markup.heading.5']
	-- ['@markup.heading.6']

	-- ['@markup.quote']
	-- ['@markup.math']

	-- ['@markup.link']
	-- ['@markup.link.label']
	-- ['@markup.link.url']

	-- ['@markup.raw']
	-- ['@markup.raw.block']

	-- ['@markup.list']
	-- ['@markup.list.checked']
	-- ['@markup.list.unchecked']

	-- ['@diff.plus']
	-- ['@diff.minus']
	-- ['@diff.delta']

	-- ['@tag']
	-- ['@tag.builtin']
	-- ['@tag.attribute']
	-- ['@tag.delimiter']

	-- Rainbow (my own extension)
	RainbowRed    = {fg='red'},
	RainbowOrange = {fg='orange'},
	RainbowYellow = {fg='yellow'},
	RainbowGreen  = {fg='green'},
	RainbowCyan   = {fg='cyan'},
	RainbowBlue   = {fg='blue'},
	RainbowViolet = {fg='violet'},

	-- -- Status line accent colours, my own extension
	StatusLineAccent        = {           fg='bg_0', bg='fg_0'},
	StatusLineAccentNormal  = {bold=true, fg='bg_0', bg='blue'},
	StatusLineAccentInsert  = {bold=true, fg='bg_0', bg='green'},
	StatusLineAccentReplace = {bold=true, fg='bg_0', bg='red'},
	StatusLineAccentVisual  = {bold=true, fg='bg_0', bg='magenta'},
	StatusLineAccentSelect  = {bold=true, fg='bg_0', bg='violet'},
	StatusLineAccentCmd     = {bold=true, fg='bg_0', bg='cyan'},
}

M.termcolors = {
	'bg_1',        -- black
	'red',         -- red
	'green',       -- green
	'yellow',      -- yellow
	'blue',        -- blue
	'magenta',     -- magenta
	'cyan',        -- cyan
	'dim_0',       -- white
	'bg_2',        -- bright black
	'br_red',      -- bright red
	'br_green',    -- bright green
	'br_yellow',   -- bright yellow
	'br_blue',     -- bright blue
	'br_magenta',  -- bright magenta
	'br_cyan',     -- bright cyan
	'fg_1',        -- bright white
}

return M
